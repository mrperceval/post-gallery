<?php
// /*
// Plugin Name:  Post Gallery
// Description:  Builds on native Wordpress gallery with direct links to gallery images.
// Version:      1.0.0
// Author:       Marlow Perceval
// License:      MIT License
// */

// add_filter('query_vars', function ($qvars) {
// 	$qvars[] = 'gallery';
// 	$qvars[] = 'slide';
// 	return $qvars;
// });

// add_action('after_setup_theme', function () {
// 	add_image_size('gallery', 860*2, 860*2, false);
// });

// //Add 'type' option to gallery settings...
// add_action('print_media_templates', function(){
// 	include( trailingslashit(dirname(__FILE__)) . 'views/admin/gallery-settings.php' );
// });


// /**
//  *
//  * Re-create the [gallery] shortcode and use thumbnails styling from Bootstrap
//  * The number of columns must be a factor of 12.
//  *
//  * @link http://getbootstrap.com/components/#thumbnails
//  */

// function gallery_tiled($attr) {
// 	$post = get_post();
// 	// $gallery_id = 0;

// 	// if (preg_match_all('/' . get_shortcode_regex() . '/s', $post->post_content, $matches, PREG_SET_ORDER)) {
// 	// 	foreach ($matches as $shortcode) {
// 	// 		if ('gallery' === $shortcode[2]) {
// 	// 			$gallery_id++;
// 	// 			$data = shortcode_parse_atts($shortcode[3]);
// 	// 			if ($attr == $data) {
// 	// 				break;
// 	// 			}
// 	// 		}
// 	// 	}
// 	// }

// 	extract(shortcode_atts([
// 		'order'      => 'ASC',
// 		'orderby'    => 'menu_order ID',
// 		'id'         => $post->ID,
// 		'gallery_id' => 1,
// 		'title'      => '',
// 		'itemtag'    => '',
// 		'icontag'    => '',
// 		'captiontag' => '',
// 		'columns'    => 3,
// 		'size'       => 'thumbnail',
// 		'include'    => '',
// 		'exclude'    => '',
// 		'link'       => ''
// 		], $attr));

// 	$post_ID = intval($id);
// 	$columns = (12 % $columns == 0) ? $columns: 3;
// 	$grid = sprintf('col-sm-%1$s col-lg-%1$s', 12/$columns);

// 	if ($order === 'RAND') {
// 		$orderby = 'none';
// 	}

// 	if (!empty($include)) {
// 		$_attachments = get_posts(['include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);

// 		$attachments = [];
// 		foreach ($_attachments as $key => $val) {
// 			$attachments[$val->ID] = $_attachments[$key];
// 		}
// 	} elseif (!empty($exclude)) {
// 		$attachments = get_children(['post_parent' => $post_ID, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);
// 	} else {
// 		$attachments = get_children(['post_parent' => $post_ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);
// 	}

// 	if (empty($attachments)) {
// 		return '';
// 	}

// 	if (is_feed()) {
// 		$output = "\n";
// 		foreach ($attachments as $att_id => $attachment) {
// 			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
// 		}
// 		return $output;
// 	}

// 	$unique = (get_query_var('page')) ? $gallery_id . '-p' . get_query_var('page'): $gallery_id;
// 	$output = '<div class="gallery gallery-' . $post_ID . '-' . $unique . '">';
// 	if (!empty($title)) {
// 		$output .= '<h3 class="gallery-title">' . $title . '</h3>';		
// 	}
// 	$output .= '<div class="gallery-items">';

// 	$i = 0;
// 	foreach ($attachments as $id => $attachment) {
// 		$image = wp_get_attachment_image($id, $size, false, ['class' => 'gallery-thumb image-placeholder']);

// 		// $output .= ($i % $columns == 0) ? '<div class="row gallery-row">': '';
// 		$output .= '<div class="gallery-item"><a href="' . get_permalink($post_ID) . '?gallery=' . $gallery_id . '&slide=' . $id . '">' . $image . '</a>';

// 		// if (trim($attachment->post_excerpt)) {
// 		// 	$output .= '<div class="caption hidden">' . wptexturize($attachment->post_excerpt) . '</div>';
// 		// }

// 		$output .= '</div>';
// 		// $i++;
// 		// $output .= ($i % $columns == 0) ? '</div>' : '';
// 	}

// 	// $output .= ($i % $columns != 0) ? '</div>' : '';
// 	$output .= '</div></div>';

// 	return $output;
// }

// function gallery($attr) {
// 	if (!empty($attr['ids'])) {
// 		if (empty($attr['orderby'])) {
// 			$attr['orderby'] = 'post__in';
// 		}
// 		$attr['include'] = $attr['ids'];
// 	}

// 	$output = apply_filters('post_gallery', '', $attr);

// 	if ($output != '') {
// 		return $output;
// 	}

// 	if (isset($attr['orderby'])) {
// 		$attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
// 		if (!$attr['orderby']) {
// 			unset($attr['orderby']);
// 		}
// 	}

// 	if (!empty($attr['type']) && $attr['type'] == 'slideshow') {
// 		return gallery_slideshow($attr);
// 	} else {
// 		return gallery_tiled($attr);
// 	}
// }

// remove_shortcode('gallery');
// add_shortcode('gallery', 'gallery');
// add_filter('use_default_gallery_style', '__return_null');

// function is_gallery() {
// 	return get_query_var('gallery') && is_single();
// }

// add_action('template_redirect', function () {
// 	if (is_gallery()) {
// 		if ($gallery_id = intval(get_query_var('gallery'))) {
// 			$slide_id = get_query_var('slide');
// 			// TODO: Regex for single $slide_id...
// 			if (!$slide_id || !get_shortcode('gallery', array('gallery_id' => $gallery_id, 'ids' => '(.*,' . $slide_id . '|.*,' . $slide_id . ',.*|' . $slide_id . ',.*)'), false, false)) {
// 				global $wp_query;
// 				$wp_query->set_404();
// 				status_header(404);
// 			}
// 		}
// 	}
// });

// add_filter('body_class', function ($classes) {
// 	if (is_gallery()) {
// 		$classes[] = 'single-gallery';
// 	}
// 	return $classes;
// });

// add_filter('the_title', function ($title) {
// 	if (is_gallery()) {
// 		$gallery_id = intval(get_query_var('gallery'));

// 		$gallery_shortcode = get_shortcode('gallery', array('gallery_id' => $gallery_id), false, false);
// 		preg_match('/(?:title="(.*?)")/', $gallery_shortcode, $output);
// 		if (!empty($output[1])) {
// 			return trim($output[1]);
// 		} else {
// 			return '';
// 		}
// 	}
// 	return $title;
// });

// add_filter('wpseo_title', function ($title) {
// 	if (is_gallery()) {
// 		$gallery_title = get_the_title();
// 		$slide_id = get_query_var('slide');
// 		$slide_title = get_post($slide_id)->post_title;

// 		$separator = wpseo_replace_vars('%%sep%%', array());
// 		$separator = ' ' . trim($separator) . ' ';

// 		$title = $slide_title . $separator . $gallery_title . $separator . $title;
// 	}
// 	return $title;
// });

// add_filter('post_thumbnail_id', function ($value, $post_id) {
// 	if (is_gallery() && ($post_id === null || $post_id == get_the_ID())) {
// 		return get_query_var('slide');
// 	}
// }, 10, 2);