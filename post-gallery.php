<?php
/*
Plugin Name:  Post Gallery
Description:  Extends native Wordpress gallery with direct links to gallery images.
Version:      1.0.0
Author:       Marlow Perceval
License:      MIT License
*/

namespace WendyRowe\PostGallery;

function load_modules() {
  // global $_wp_theme_features;
  foreach (glob(__DIR__ . '/modules/*.php', GLOB_NOSORT) as $file) {
    // $feature = 'soil-' . basename($file, '.php');
    // if (isset($_wp_theme_features[$feature])) {
      // Options::init($feature, $_wp_theme_features[$feature]);
      require_once $file;
    // }
  }
}
add_action('after_setup_theme', __NAMESPACE__ . '\\load_modules');
