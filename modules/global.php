<?php

function is_gallery() {
	return get_query_var('gallery') && is_single();
}