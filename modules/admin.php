<?php

namespace WendyRowe\PostGallery\Admin;

if(is_admin()){
	//Add 'type' option to gallery settings...
	add_action('print_media_templates', function(){
		require_once(__DIR__  . '/../views/admin/gallery-settings.php' );
	});
}