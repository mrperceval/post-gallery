<?php

namespace WendyRowe\PostGallery\GalleryPage;

if (is_admin()) {
	return;
}

add_filter('query_vars', function ($qvars) {
	$qvars[] = 'gallery';
	$qvars[] = 'slide';
	return $qvars;
});

add_action('template_redirect', function () {
	if (is_gallery()) {
		if ($gallery_id = intval(get_query_var('gallery'))) {
			$slide_id = get_query_var('slide');
			// TODO: Regex for single $slide_id...
			if (!$slide_id || !get_shortcode('gallery', array('gallery_id' => $gallery_id, 'ids' => '(.*,' . $slide_id . '|.*,' . $slide_id . ',.*|' . $slide_id . ',.*)'), false, false)) {
				global $wp_query;
				$wp_query->set_404();
				status_header(404);
			}
		}
	}
});

add_filter('body_class', function ($classes) {
	if (is_gallery()) {
		$classes[] = 'single-gallery';
	}
	return $classes;
});

add_filter('the_title', function ($title) {
	if (is_gallery()) {
		$gallery_id = intval(get_query_var('gallery'));

		$gallery_shortcode = get_shortcode('gallery', array('gallery_id' => $gallery_id), false, false);
		preg_match('/(?:title="(.*?)")/', $gallery_shortcode, $output);
		if (!empty($output[1])) {
			return trim($output[1]);
		} else {
			return '';
		}
	}
	return $title;
});

add_filter('wpseo_title', function ($title) {
	if (is_gallery()) {
		$gallery_title = get_the_title();
		$slide_id = get_query_var('slide');
		$slide_title = get_post($slide_id)->post_title;

		$separator = wpseo_replace_vars('%%sep%%', array());
		$separator = ' ' . trim($separator) . ' ';

		if ($gallery_title) {
			$title = $gallery_title . $separator . $title;
		}

		if ($slide_title) {
			$title = $slide_title . $separator . $title;
		}
	}
	return $title;
});

add_filter('wpseo_canonical', function ($url) {
	if (is_gallery()) {
		return $url . '?gallery=' . intval(get_query_var('gallery')) . '&slide=' . get_query_var('slide');
	}
	return $url;
});

add_filter('post_thumbnail_id', function ($value, $post_id) {
	if (is_gallery() && ($post_id === null || $post_id == get_the_ID())) {
		return get_query_var('slide');
	}
}, 10, 2);
