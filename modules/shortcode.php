<?php

namespace WendyRowe\PostGallery\Shortcode;

if (!is_blog_installed()) { return; }

class Shortcode {
  private static $_single; // Let's make this a singleton.

  public function __construct() {
    if (isset(self::$_single)) { return; }

    self::$_single = $this; // Singleton set.

    remove_shortcode('gallery');
    add_shortcode('gallery', array($this, 'gallery'));
    add_filter('use_default_gallery_style', '__return_null');
  }

  public function gallery($attr) {
    if (!empty($attr['ids'])) {
      if (empty($attr['orderby'])) {
        $attr['orderby'] = 'post__in';
      }
      $attr['include'] = $attr['ids'];
    }

    $output = apply_filters('post_gallery', '', $attr);

    if ($output != '') {
      return $output;
    }

    if (isset($attr['orderby'])) {
      $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
      if (!$attr['orderby']) {
        unset($attr['orderby']);
      }
    }

    if (is_feed()) {
        return $this->gallery_feed($attr);
    } else {
      if (empty($attr['type'])) {
        return $this->gallery_tiled($attr);
      }

      $gallery_func = 'gallery_' . $attr['type'];
      if (method_exists($this, $gallery_func)) {
        return $this->$gallery_func($attr);
      }
      return $this->gallery_tiled($attr);
    }
  }

  private function gallery_feed($attr) {
    $post = get_post();

    extract(shortcode_atts([
      'order'      => 'ASC',
      'orderby'    => 'menu_order ID',
      'id'         => $post->ID,
      'gallery_id' => 1,
      'title'      => '',
      'itemtag'    => '',
      'icontag'    => '',
      'captiontag' => '',
      'columns'    => 3,
      'size'       => 'thumbnail',
      'include'    => '',
      'exclude'    => '',
      'link'       => ''
      ], $attr, 'gallery_feed'));

    $post_ID = intval($id);

    if ($order === 'RAND') {
      $orderby = 'none';
    }

    if (!empty($include)) {
      $_attachments = get_posts(['include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);

      $attachments = [];
      foreach ($_attachments as $key => $val) {
        $attachments[$val->ID] = $_attachments[$key];
      }
    } elseif (!empty($exclude)) {
      $attachments = get_children(['post_parent' => $post_ID, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);
    } else {
      $attachments = get_children(['post_parent' => $post_ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);
    }

    if (empty($attachments)) {
      return '';
    }

    $output = "\n";
    foreach ($attachments as $att_id => $attachment) {
      $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
    }
    return $output;
  }

  private function gallery_slideshow($attr) {
    $post = get_post();

    extract(shortcode_atts([
      'type'       => '',
      'order'      => 'ASC',
      'orderby'    => 'menu_order ID',
      'id'         => $post->ID,
      'gallery_id' => 1,
      'title'      => '',
      'square_crop' => false,
      'itemtag'    => '',
      'icontag'    => '',
      'captiontag' => '',
      'columns'    => 3,
      'size'       => 'medium',
      'include'    => '',
      'exclude'    => '',
      'link'       => ''
      ], $attr, 'gallery_slideshow'));

    $post_ID = intval($id);

    if ($order === 'RAND') {
      $orderby = 'none';
    }

    if (!empty($include)) {
      $attachment_IDs = explode(',', $include);
      list($first_attachment_ID) = $attachment_IDs;

      $attachments = get_posts([
        'include' => $first_attachment_ID,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'order' => $order,
        'orderby' => $orderby
      ]);
    } elseif (!empty($exclude)) {
      $attachments = get_children([
        'numberposts' => 1,
        'post_parent' => $post_ID,
        'exclude' => $exclude,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'order' => $order,
        'orderby' => $orderby
      ]);
    } else {
      $attachments = get_children([
        'numberposts' => 1,
        'post_parent' => $post_ID,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'order' => $order,
        'orderby' => $orderby
      ]);
    }

    if (empty($attachments)) {
      return '';
    }
    $first_attachment = $attachments[0];

    $unique = (get_query_var('page')) ? $gallery_id . '-p' . get_query_var('page'): $gallery_id;
    $output = '<div class="gallery gallery-' . $post_ID . '-' . $unique . ' gallery-slideshow' . ($square_crop ? ' square-cropped' : '') . '">';
    $output .= '<div class="gallery-first-item">';
    if (trim($first_attachment->post_excerpt)) {
      $output .= '<figure class="caption">';
    }
    $image = wp_get_attachment_image($first_attachment_ID, $size, false, ['class' => 'gallery-thumb image-placeholder']);
    $output .= '<a class="entry-content-image" href="' . get_permalink($post_ID) . '?gallery=' . $gallery_id . '&slide=' . $first_attachment_ID . '">';
    $output .= $image;
    $output .= '<div class="gallery-count">' . count($attachment_IDs) . '</div>';
    $output .= '</a>';
    if (trim($first_attachment->post_excerpt)) {
      $output .= '<figcaption>' . wptexturize($first_attachment->post_excerpt) . '</figcaption></figure>';
    }
    $output .= '</div></div>';

    return $output;
  }

  private function gallery_tiled($attr) {
    $post = get_post();

    extract(shortcode_atts([
      'type'       => '',
      'order'      => 'ASC',
      'orderby'    => 'menu_order ID',
      'id'         => $post->ID,
      'gallery_id' => 1,
      'link_to_gallery_page' => false,
      'background' => false,
      'title_wrap' => false,
      'title'      => '',
      'itemtag'    => '',
      'icontag'    => '',
      'captiontag' => '',
      'columns'    => 3,
      'size'       => 'thumbnail',
      'include'    => '',
      'exclude'    => '',
      'link'       => ''
      ], $attr, 'gallery_tiled'));

    $post_ID = intval($id);

    if ($order === 'RAND') {
      $orderby = 'none';
    }

    if (!empty($include)) {
      $_attachments = get_posts(['include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);

      $attachments = [];
      foreach ($_attachments as $key => $val) {
        $attachments[$val->ID] = $_attachments[$key];
      }
    } elseif (!empty($exclude)) {
      $attachments = get_children(['post_parent' => $post_ID, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);
    } else {
      $attachments = get_children(['post_parent' => $post_ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);
    }

    if (empty($attachments)) {
      return '';
    }

    $unique = (get_query_var('page')) ? $gallery_id . '-p' . get_query_var('page'): $gallery_id;

    $gallery_classnames = array(
      'gallery',
      'gallery-' . $post_ID . '-' . $unique,
      'gallery-tiled'
    );

    if ($columns == 2) {
      $gallery_classnames[] = 'column-2';
    } else if ($columns == 3) {
      $gallery_classnames[] = 'column-3';
    } else {
      $title_wrap = false;
    }
    if ($background) {
      $gallery_classnames[] = 'gallery-background';
    }
    if ($title_wrap) {
      $gallery_classnames[] = 'title-wrap';
    }

    $output = '<div class="' . implode(' ', $gallery_classnames) . '">';
    if (!empty($title)) {
      $output .= '<h3 class="gallery-title' . ($title_wrap ? ' h2 stamp' : '') . '">' . $title . '</h3>';
    }

    $output .= '<div class="gallery-items">';

    $i = 0;
    foreach ($attachments as $id => $attachment) {
      $image = wp_get_attachment_image($id, $size, false, ['class' => 'gallery-thumb image-placeholder']);
      $output .= '<div class="gallery-item' . ($title_wrap && !$i ? ' stamp': '') . '">';
      if ($link_to_gallery_page) {
        $output .= '<a href="' . get_permalink($post_ID) . '?gallery=' . $gallery_id . '&slide=' . $id . '">';
      }
      $output .= $image;
      if ($link_to_gallery_page) {
        $output .= '</a>';
      }
      $output .= '</div>';
      $i++;
    }
    $output .= '</div></div>';
    return $output;
  }

  private function gallery_cascade($attr) {
    $post = get_post();

    extract(shortcode_atts([
      'type'       => '',
      'order'      => 'ASC',
      'orderby'    => 'menu_order ID',
      'id'         => $post->ID,
      'gallery_id' => 1,
      'link_to_gallery_page' => false,
      'numbered' => false,
      'title'      => '',
      'itemtag'    => '',
      'icontag'    => '',
      'captiontag' => '',
      'columns'    => 3,
      'size'       => 'thumbnail',
      'include'    => '',
      'exclude'    => '',
      'link'       => ''
      ], $attr, 'gallery_cascade'));

    $post_ID = intval($id);

    if ($order === 'RAND') {
      $orderby = 'none';
    }

    if (!empty($include)) {
      $_attachments = get_posts(['include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);

      $attachments = [];
      foreach ($_attachments as $key => $val) {
        $attachments[$val->ID] = $_attachments[$key];
      }
    } elseif (!empty($exclude)) {
      $attachments = get_children(['post_parent' => $post_ID, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);
    } else {
      $attachments = get_children(['post_parent' => $post_ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby]);
    }

    if (empty($attachments)) {
      return '';
    }

    $unique = (get_query_var('page')) ? $gallery_id . '-p' . get_query_var('page'): $gallery_id;
    $no_title = empty($title);
    $output = '<div class="gallery gallery-' . $post_ID . '-' . $unique . ' gallery-cascade' . ($no_title ? ' no-title' : '') . ($numbered ? ' numbered' : '') . '">';
    if (!$no_title) {
      $output .= '<h3 class="gallery-title h2">' . $title . '</h3>';
    }
    $output .= '<div class="gallery-items">';

    $i = 0;
    foreach ($attachments as $id => $attachment) {
      if ($i % 2 == 0) {
        if ($no_title) {
          $align = 'left';
        } else {
          $align = 'right';
        }
      } else {
        if ($no_title) {
          $align = 'right';
        } else {
          $align = 'left';
        }
      }

      $has_title = trim($attachment->post_title);
      $has_content = trim($attachment->post_content);
      $has_excerpt = trim($attachment->post_excerpt);

      $has_text = ($has_title || $has_content || $has_excerpt);

      $output .= '<figure class="gallery-item align-' . $align . ' ' . ($has_text ? 'with-text' : 'without-text') . '">';
      $image = wp_get_attachment_image($id, $size, false, ['class' => 'gallery-thumb image-placeholder']);

      $imag_wrap = '<div class="gallery-item-image cascade-item">';
      if ($link_to_gallery_page) {
        $imag_wrap .= '<a class="entry-content-image" href="' . get_permalink($post_ID) . '?gallery=' . $gallery_id . '&slide=' . $id . '">';
      } else {
        $imag_wrap .= '<div class="entry-content-image">';
      }
      $imag_wrap .= $image;
      if ($link_to_gallery_page) {
        $imag_wrap .= '</a>';
      } else {
        $imag_wrap .= '</div>';
      }
      $imag_wrap .= '</div>';

      $figcaption = '<figcaption class="gallery-item-caption cascade-item"><div class="caption-content">';
      if ($has_title) {
        $figcaption .= '<h4 class="gallery-image-title h3">' . wptexturize($attachment->post_title) . '</h4>';
      }
      if ($has_content) {
        $figcaption .= apply_filters('the_content', $attachment->post_content);
      }
      if ($has_excerpt) {
        $figcaption .= '<p class="caption-text">' . wptexturize($attachment->post_excerpt) . '</p>';
      }
      $figcaption .= '</div></figcaption>';

      if ($i % 2 == 0) {
        if ($no_title) {
          $output .= $imag_wrap;
          $output .= $figcaption;
        } else {
          $output .= $figcaption;
          $output .= $imag_wrap;
        }
      } else {
        if ($no_title) {
          $output .= $figcaption;
          $output .= $imag_wrap;
        } else {
          $output .= $imag_wrap;
          $output .= $figcaption;
        }
      }

      $output .= '</figure>';

      $i++;
    }
    $output .= '</div></div>';

    return $output;
  }
}

new Shortcode();
