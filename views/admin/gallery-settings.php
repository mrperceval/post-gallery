<?php

// define your backbone template;
// the "tmpl-" prefix is required,
// and your input field should have a data-setting attribute
// matching the shortcode name

?>

<script type="text/html" id="tmpl-wr-gallery-settings">
	<label class="setting">
		<span class="name"><?php _e('Title'); ?></span>
		<input type="text" data-setting="title">
	</label>
	<label class="setting">
		<span class="name"><?php _e('Type'); ?></span>
		<select data-setting="type">
			<option value="tiled">Tiled</option>
			<option value="slideshow">Slideshow</option>
			<option value="cascade">Cascade</option>
		</select>
	</label>
	<label class="setting">
		<span class="name"><?php _e('Link to Gallery Page'); ?></span>
		<input type="checkbox" data-setting="link_to_gallery_page">
	</label>
	<label class="setting">
		<span class="name"><?php _e('Columns'); ?></span>
		<select data-setting="columns">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
	</label>
	<label class="setting">
		<span class="name"><?php _e('Background'); ?></span>
		<input type="checkbox" data-setting="background">
	</label>
	<label class="setting">
		<span class="name"><?php _e('Title Wrap'); ?></span>
		<input type="checkbox" data-setting="title_wrap">
	</label>
	<label class="setting">
		<span class="name"><?php _e('Numbered'); ?></span>
		<input type="checkbox" data-setting="numbered">
	</label>
	<label class="setting">
		<span class="name"><?php _e('Square Crop'); ?></span>
		<input type="checkbox" data-setting="square_crop">
	</label>
</script>

<script>
jQuery(document).ready(function(){

	_.extend(wp.media.gallery.defaults, {
		type: 'tiled',
		link_to_gallery_page: false,
		background: false,
		title_wrap: false,
		numbered: false,
		square_crop: false
	});
	// console.log(wp.media.gallery);
	// wp.media.collection = wp.media.collection({
	// 	shortcode: function( attachments ) {
	// 		var props = attachments.props.toJSON(),
	// 			attrs = _.pick( props, 'orderby', 'order' ),
	// 			shortcode, clone;

	// 		if ( attachments.type ) {
	// 			attrs.type = attachments.type;
	// 			delete attachments.type;
	// 		}

	// 		if ( attachments[this.tag] ) {
	// 			_.extend( attrs, attachments[this.tag].toJSON() );
	// 		}

	// 		// Convert all gallery shortcodes to use the `ids` property.
	// 		// Ignore `post__in` and `post__not_in`; the attachments in
	// 		// the collection will already reflect those properties.
	// 		attrs.ids = attachments.pluck('id');

	// 		// Copy the `uploadedTo` post ID.
	// 		if ( props.uploadedTo ) {
	// 			attrs.id = props.uploadedTo;
	// 		}
	// 		// Check if the gallery is randomly ordered.
	// 		delete attrs.orderby;

	// 		if ( attrs._orderbyRandom ) {
	// 			attrs.orderby = 'rand';
	// 		} else if ( attrs._orderByField && attrs._orderByField != 'rand' ) {
	// 			attrs.orderby = attrs._orderByField;
	// 		}

	// 		delete attrs._orderbyRandom;
	// 		delete attrs._orderByField;

	// 		// If the `ids` attribute is set and `orderby` attribute
	// 		// is the default value, clear it for cleaner output.
	// 		if ( attrs.ids && 'post__in' === attrs.orderby ) {
	// 			delete attrs.orderby;
	// 		}

	// 		console.log(attrs);
	// 		if (!attrs.gallery_id) {
	// 			var date = new Date();
	// 			var components = [
	// 				// date.getYear(),
	// 				// date.getMonth(),
	// 				// date.getDate(),
	// 				// date.getHours(),
	// 				date.getMinutes(),
	// 				date.getSeconds(),
	// 				date.getMilliseconds()
	// 			];
	// 			attrs.gallery_id = components.join('');
	// 		}
	// 		console.log(attrs);

	// 		attrs = this.setDefaults( attrs );

	// 		shortcode = new wp.shortcode({
	// 			tag:    this.tag,
	// 			attrs:  attrs,
	// 			type:   'single'
	// 		});

	// 		// Use a cloned version of the gallery.
	// 		clone = new wp.media.model.Attachments( attachments.models, {
	// 			props: props
	// 		});
	// 		clone[ this.tag ] = attachments[ this.tag ];
	// 		collections[ shortcode.string() ] = clone;

	// 		return shortcode;
	// 	}
	// });
	// wp.media.collection = function(attributes) {
	// 	var collections = {};

	// 	wp.media.collection.shortcode = function( attachments ) {
	// 		var props = attachments.props.toJSON(),
	// 			attrs = _.pick( props, 'orderby', 'order' ),
	// 			shortcode, clone;

	// 		if ( attachments.type ) {
	// 			attrs.type = attachments.type;
	// 			delete attachments.type;
	// 		}

	// 		if ( attachments[this.tag] ) {
	// 			_.extend( attrs, attachments[this.tag].toJSON() );
	// 		}

	// 		// Convert all gallery shortcodes to use the `ids` property.
	// 		// Ignore `post__in` and `post__not_in`; the attachments in
	// 		// the collection will already reflect those properties.
	// 		attrs.ids = attachments.pluck('id');

	// 		// Copy the `uploadedTo` post ID.
	// 		if ( props.uploadedTo ) {
	// 			attrs.id = props.uploadedTo;
	// 		}
	// 		// Check if the gallery is randomly ordered.
	// 		delete attrs.orderby;

	// 		if ( attrs._orderbyRandom ) {
	// 			attrs.orderby = 'rand';
	// 		} else if ( attrs._orderByField && attrs._orderByField != 'rand' ) {
	// 			attrs.orderby = attrs._orderByField;
	// 		}

	// 		delete attrs._orderbyRandom;
	// 		delete attrs._orderByField;

	// 		// If the `ids` attribute is set and `orderby` attribute
	// 		// is the default value, clear it for cleaner output.
	// 		if ( attrs.ids && 'post__in' === attrs.orderby ) {
	// 			delete attrs.orderby;
	// 		}

	// 		console.log(attrs);
	// 		if (!attrs.gallery_id) {
	// 			var date = new Date();
	// 			var components = [
	// 				// date.getYear(),
	// 				// date.getMonth(),
	// 				// date.getDate(),
	// 				// date.getHours(),
	// 				date.getMinutes(),
	// 				date.getSeconds(),
	// 				date.getMilliseconds()
	// 			];
	// 			attrs.gallery_id = components.join('');
	// 		}
	// 		console.log(attrs);

	// 		attrs = this.setDefaults( attrs );

	// 		shortcode = new wp.shortcode({
	// 			tag:    this.tag,
	// 			attrs:  attrs,
	// 			type:   'single'
	// 		});

	// 		// Use a cloned version of the gallery.
	// 		clone = new wp.media.model.Attachments( attachments.models, {
	// 			props: props
	// 		});
	// 		clone[ this.tag ] = attachments[ this.tag ];
	// 		collections[ shortcode.string() ] = clone;

	// 		return shortcode;
	// 	};

	// 	return _.extend( wp.media.collection, attributes );
	// };

	var collections = {};
	_.extend( wp.media.gallery, {
		shortcode: function( attachments ) {
			var props = attachments.props.toJSON(),
				attrs = _.pick( props, 'orderby', 'order' ),
				shortcode, clone;

			if ( attachments.type ) {
				attrs.type = attachments.type;
				delete attachments.type;
			}

			if ( attachments[this.tag] ) {
				_.extend( attrs, attachments[this.tag].toJSON() );
			}

			// Convert all gallery shortcodes to use the `ids` property.
			// Ignore `post__in` and `post__not_in`; the attachments in
			// the collection will already reflect those properties.
			attrs.ids = attachments.pluck('id');

			// Copy the `uploadedTo` post ID.
			if ( props.uploadedTo ) {
				attrs.id = props.uploadedTo;
			}
			// Check if the gallery is randomly ordered.
			delete attrs.orderby;

			if ( attrs._orderbyRandom ) {
				attrs.orderby = 'rand';
			} else if ( attrs._orderByField && attrs._orderByField != 'rand' ) {
				attrs.orderby = attrs._orderByField;
			}

			delete attrs._orderbyRandom;
			delete attrs._orderByField;

			// If the `ids` attribute is set and `orderby` attribute
			// is the default value, clear it for cleaner output.
			if ( attrs.ids && 'post__in' === attrs.orderby ) {
				delete attrs.orderby;
			}

			if (!attrs.gallery_id) {
				var currentDate = new Date();
				var components = [
					// currentDate.getYear(),
					// currentDate.getMonth(),
					// currentDate.getDate(),
					// currentDate.getHours(),
					currentDate.getMinutes(),
					currentDate.getSeconds(),
					currentDate.getMilliseconds()
				];
				attrs.gallery_id = parseInt(components.join(''), 10);
			}

			attrs = this.setDefaults( attrs );

			shortcode = new wp.shortcode({
				tag:    this.tag,
				attrs:  attrs,
				type:   'single'
			});

			// Use a cloned version of the gallery.
			clone = new wp.media.model.Attachments( attachments.models, {
				props: props
			});
			clone[ this.tag ] = attachments[ this.tag ];
			collections[ shortcode.string() ] = clone;

			return shortcode;
		}
	});

	// wp.media.controller.GalleryEdit = wp.media.controller.GalleryEdit.extend({
	// 	setGalleryIdOnUpdate: function() {
	// 		var setGalleryId = function () {
	// 			if (!selection.gallery.attributes.gallery_id) {
	// 				var date = new Date();
	// 				var components = [
	// 					// date.getYear(),
	// 					// date.getMonth(),
	// 					// date.getDate(),
	// 					// date.getHours(),
	// 					date.getMinutes(),
	// 					date.getSeconds(),
	// 					date.getMilliseconds()
	// 				];
	// 				selection.gallery.attributes.gallery_id = components.join('')
	// 			}
	// 		};

	// 		// this.frame.state('gallery-add').on( 'update', function( selection ) {
	// 		// 	console.log('gallery-add');
	// 		// 	setGalleryId();
	// 		// } );
	// 		this.frame.state('gallery-edit').on( 'update', function( selection ) {
	// 			console.log('gallery-edit', selection);
	// 			if (!selection.gallery.attributes.gallery_id) {
	// 				var date = new Date();
	// 				var components = [
	// 					// date.getYear(),
	// 					// date.getMonth(),
	// 					// date.getDate(),
	// 					// date.getHours(),
	// 					date.getMinutes(),
	// 					date.getSeconds(),
	// 					date.getMilliseconds()
	// 				];
	// 				// wp.media.collection.set('gallery_id', components.join(''));
	// 				selection.gallery.attributes.gallery_id = components.join('')
	// 				console.log(wp.media.gallery);
	// 			}
	// 		} );
	// 	},
	// 	activate: function() {
	// 		var library = this.get('library');

	// 		// Limit the library to images only.
	// 		library.props.set( 'type', 'image' );

	// 		// Watch for uploaded attachments.
	// 		this.get('library').observe( wp.Uploader.queue );

	// 		this.frame.on( 'content:render:browse', this.gallerySettings, this );
	// 		this.frame.on( 'content:render:browse', this.setGalleryIdOnUpdate, this );

	// 		wp.media.controller.Library.prototype.activate.apply( this, arguments );
	// 	}
	// });

	// merge default gallery settings template with yours
	wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
		template: function(view){
			// return wp.media.template('gallery-settings')(view)
			// + wp.media.template('wr-gallery-settings')(view);
			return '<h3>Gallery Settings</h3>' + wp.media.template('wr-gallery-settings')(view);
		}
	});
});
</script>
